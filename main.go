package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
	"os"
)

func timeHandler(w http.ResponseWriter, req *http.Request) {
	if req.Method == http.MethodGet {
		fmt.Fprintf(w, time.Now().Format("15h04"))
	}
}

func addHandler(w http.ResponseWriter, req *http.Request) {
	if req.Method == http.MethodPost {
		if err := req.ParseForm(); err != nil {
			fmt.Println("Something went bad")
			fmt.Fprintln(w, "Something went bad")
			return
		}

		var author = req.PostForm.Get("author")
		var entry = req.PostForm.Get("entry")

		file, err := os.OpenFile("entries.txt", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0600)
		defer file.Close()

		_, err = file.WriteString(req.PostForm.Get("entry") + "\n")
		if err != nil {
			panic(err)
		}

		fmt.Fprintf(w, author + " : " + entry)
	}
}

func entriesHandler(w http.ResponseWriter, req *http.Request) {
	if req.Method == http.MethodGet {
		file, err := os.OpenFile("entries.txt", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0600)
		defer file.Close()

		data, err := ioutil.ReadFile("entries.txt")
		if err != nil {
			fmt.Println(err)
		}

		fmt.Fprintf(w, string(data))
	}
}

func main() {
	http.HandleFunc("/", timeHandler)
	http.HandleFunc("/add", addHandler)
	http.HandleFunc("/entries", entriesHandler)
	http.ListenAndServe(":4567", nil)
}